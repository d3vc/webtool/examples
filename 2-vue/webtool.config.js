const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
	appEntry: './src/entry.js',
	configureEncore(encore) {
		encore.enableVueLoader(() => {}, {
			runtimeCompilerBuild: false,
			version: 3
		});

		encore.enableNetlifyRedirects({
			'/api/proxy/*': 'https://',
			'/api/get': 'https://httpbin.org/get',
			'/api/anything': 'https://httpbin.org/anything',
			'/api/anything/*': {
				dest: 'https://httpbin.org/anything/',
				headers: {
					'X-Custom-Header': 'COOL',
					'X-Additionnal-Header': 'COOLER'
				}
			}
		});

		encore.configureDefines({
			__VUE_OPTIONS_API__: true,
			__VUE_PROD_DEVTOOLS__: false
		});
	},
	configureWebpack(webpack) {
		webpack.plugins.push(new FaviconsWebpackPlugin('./src/assets/logo.png'));
	},
	configureXO(xo) {
		xo.extends.push('plugin:vue/vue3-recommended');
	}
};
